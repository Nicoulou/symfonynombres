<?php

use PHPUnit\Framework\TestCase;

use App\Utils\ConvertNumber;
use App\Utils\ConvertNumberException;

class ConvertTest extends TestCase {

    //Test constructeur
    public function test_ConvertNumber_SansParam_Construit(){
        $convert = new ConvertNumber();
        $this->assertTrue(true);
    }

    //Si la base demande est supérieur à 16
    public function test_ConvertNumberBaseNonGere_2Param_RenvoieException(){
        $this->expectException(ConvertNumberException::class);
        $convert = new ConvertNumber();
        $convert->convert("58", 17);
    }

    //Si le premier argument n'est pas un string
    public function test_ConvertNumberNotString_2Param_RenvoieException(){
        $this->expectException(ConvertNumberException::class);
        $convert = new ConvertNumber();
        $convert->convert(58, 12);
    }

    //Test conversion nombre en base 2
    public function test_ConvertNumberBase2_2Param_RenvoieNombreBase2(){
        $convert = new ConvertNumber();
        $this-> assertEquals("111010", $convert->convert("58", 2));
    }

    //Test conversion nombre en base 16
    public function test_ConvertNumberBase16_2Param_RenvoieNombreBase16(){
        $convert = new ConvertNumber();
        $this-> assertEquals("3A", $convert->convert("58", 16));
    }

    //Test conversion en base 10 depuis la base 2
    public function test_ConvertNumberBase10DepuisBase2_2Param_RenvoieNombreBase10(){
        $convert = new ConvertNumber();
        $this->assertEquals("58", $convert->convertBase10("111010", 2));
    }

    //Test conversion en base 10 depuis la base 16
    public function test_ConvertNumberBase10DepuisBase16_2Param_RenvoieNombreBase10(){
        $convert = new ConvertNumber();
        $this->assertEquals("58", $convert->convertBase10("3A", 16));
    }

    //Si le premier argument n'est pas un string
    public function test_ConvertBase10NotString_2Param_RenvoieException(){
        $this->expectException(ConvertNumberException::class);
        $convert = new ConvertNumber();
        $convert->convertBase10(38, 16);
    }

    //Si la base demande est supérieur à 16
    public function test_ConvertBase10BaseNonGere_2Param_RenvoieException(){
        $this->expectException(ConvertNumberException::class);
        $convert = new ConvertNumber();
        $convert->convertBase10("58", 17);
    }


}