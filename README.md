Liste des routes

/decToBaseX/{nombre}/{base}
Convertir un nombre en décimale dans une base entre 2 et 16
Exemple : /decToBaseX/58/2

/baseXToDec/{nombre}/{baseOrigine}
Convertir un nombre en base X en decimale 
Exemple : /baseXToDec/3A/16