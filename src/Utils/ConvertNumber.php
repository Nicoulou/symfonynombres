<?php

namespace App\Utils;

class ConvertNumber {

    public function convert($nombre, $base){
        $bin = "";
        if (($base < 2) || ($base > 16) || (gettype($nombre) != "string")){
            throw new ConvertNumberException("Erreur");
        }
        $nombre = intval($nombre);
        if($nombre==0)
            return "0";
        while($nombre > 0)
        {
            $bin = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F" ][($nombre%$base)].$bin;
            $nombre = floor($nombre/$base);
        }
        return $bin;
    }

    public function convertBase10($nombre, $base){
        $valeur = 0;
        if (($base < 2) || ($base > 16) || (gettype($nombre) != "string")){
            throw new ConvertNumberException("Erreur");
        }
        for ($i = 0; $i < strlen($nombre); $i++){
            $valeur += $this->getValeur($nombre[strlen($nombre)-$i-1]) * pow($base, $i);
        }
        return $valeur;
    }

    public function getValeur($item){
        switch (strtoupper($item)){
            case '0':
                return 0;
            case '1':
                return 1;
            case '2':
                return 2;
            case '3':
                return 3;
            case '4':
                return 4;
            case '5':
                return 5;
            case '6':
                return 6;
            case '7':
                return 7;
            case '8':
                return 8;
            case '9':
                return 9;
            case 'A':
                return 10;
            case 'B':
                return 11;
            case 'C':
                return 12;
            case 'D':
                return 13;
            case 'E':
                return 14;
            case 'F':
                return 15;
        }
    }
    
}