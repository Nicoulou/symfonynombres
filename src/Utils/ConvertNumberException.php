<?php

namespace App\Utils;

class ConvertNumberException extends \Exception {

    public function construct($message, $code = 0){
        parent::__construct($message, $code);
    }

    public function __ToString() {
        return $this->message;
    }

}