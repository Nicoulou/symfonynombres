<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\Routing\Annotation\Route;

use App\Utils\ConvertNumber;
use App\Utils\ConvertNumberException;

class ConvertController extends AbstractController {
    /**
     * @Route("/decToBaseX/{nombre}/{base}")
     */
    public function decToBaseX($nombre, $base){
        $convert = new ConvertNumber();
        $converti = $convert->convert($nombre, intval($base));
        return $this->render('base.html.twig', ["origine" => $nombre, "converti" => $converti]);
    }

    /**
     * @Route("/baseXToDec/{nombre}/{baseOrigine}")
     */
    public function baseXToDec($nombre, $baseOrigine){
        $convert = new ConvertNumber();
        $converti = $convert->convertBase10($nombre, intval($baseOrigine));
        return $this->render('base.html.twig', ["origine" => $nombre, "converti" => $converti]);
    }
}